from dogpile.cache.region import _backend_loader
from ._fixtures import _GenericBackendTest, _GenericMutexTest
from . import eq_
from unittest import TestCase
from mock import patch, Mock
import pytest


class _TestRedisConn(object):

    @classmethod
    def _check_backend_available(cls, backend):
        try:
            client = backend._create_client()
            client.set("x", "y")
            # on py3k it appears to return b"y"
            assert client.get("x").decode("ascii") == "y"
            client.delete("x")
        except:
            pytest.skip(
                "redis is not running or "
                "otherwise not functioning correctly")


class RedisTest(_TestRedisConn, _GenericBackendTest):
    backend = 'dogpile.cache.redis'
    config_args = {
        "arguments": {
            'host': '127.0.0.1',
            'port': 6379,
            'db': 0,
        }
    }


class RedisDistributedMutexTest(_TestRedisConn, _GenericMutexTest):
    backend = 'dogpile.cache.redis'
    config_args = {
        "arguments": {
            'host': '127.0.0.1',
            'port': 6379,
            'db': 0,
            'distributed_lock': True,
        }
    }


@patch('redis.StrictRedis', autospec=True)
class RedisConnectionTest(TestCase):
    backend = 'dogpile.cache.redis'

    @classmethod
    def setup_class(cls):
        try:
            cls.backend_cls = _backend_loader.load(cls.backend)
            cls.backend_cls({})
        except ImportError:
            pytest.skip("Backend %s not installed" % cls.backend)

    def _test_helper(self, mock_obj, expected_args, connection_args=None):
        if connection_args is None:
            # The redis backend pops items from the dict, so we copy
            connection_args = expected_args.copy()

        self.backend_cls(connection_args)
        mock_obj.assert_called_once_with(**expected_args)

    def test_connect_with_defaults(self, MockStrictRedis):
        # The defaults, used if keys are missing from the arguments dict.
        arguments = {
            'host': 'localhost',
            'password': None,
            'port': 6379,
            'db': 0,
        }
        self._test_helper(MockStrictRedis, arguments, {})

    def test_connect_with_basics(self, MockStrictRedis):
        arguments = {
            'host': '127.0.0.1',
            'password': None,
            'port': 6379,
            'db': 0,
        }
        self._test_helper(MockStrictRedis, arguments)

    def test_connect_with_password(self, MockStrictRedis):
        arguments = {
            'host': '127.0.0.1',
            'password': 'some password',
            'port': 6379,
            'db': 0,
        }
        self._test_helper(MockStrictRedis, arguments)

    def test_connect_with_socket_timeout(self, MockStrictRedis):
        arguments = {
            'host': '127.0.0.1',
            'port': 6379,
            'socket_timeout': 0.5,
            'password': None,
            'db': 0,
        }
        self._test_helper(MockStrictRedis, arguments)

    def test_connect_with_connection_pool(self, MockStrictRedis):
        pool = Mock()
        arguments = {
            'connection_pool': pool,
            'socket_timeout': 0.5
        }
        expected_args = {'connection_pool': pool}
        self._test_helper(MockStrictRedis, expected_args,
                          connection_args=arguments)

    def test_connect_with_url(self, MockStrictRedis):
        arguments = {
            'url': 'redis://redis:password@127.0.0.1:6379/0'
        }
        self._test_helper(MockStrictRedis.from_url, arguments)

class _TestRedisClusterConn(object):

    @classmethod
    def _check_backend_available(cls, backend):
        try:
            client = backend._create_client()
            client.set("x", "y")
            # on py3k it appears to return b"y"
            assert client.get("x").decode("ascii") == "y"
            client.delete("x")
        except:
            pytest.skip(
                "redis is not running or "
                "otherwise not functioning correctly")


class RedisClusterTest(_TestRedisClusterConn, _GenericBackendTest):
    backend = 'dogpile.cache.rediscluster'
    config_args = {
        "arguments": {
            'host': '127.0.0.1',
            'port': 7000,
        }
    }

from dogpile.cache.backends.redis import RedisClusterBackend

class MockRedisClusterBackend(RedisClusterBackend):

    def _imports(self):
        pass

    def _create_client(self):
        client = None
        if self.url:
            client = MockClient(self.url)
        else:
            client = MockClient(self.host, self.password, self.port, )
        return client

class MockClient(object):
    number_of_clients = 0

    def __init__(self, *arg, **kw):
        self.arg = arg
        self.kw = kw
        self.canary = []
        self._cache = {}
        MockClient.number_of_clients += 1

    def get(self, key):
        return self._cache.get(key)

    def set(self, key, value, **kw):
        self.canary.append(kw)
        self._cache[key] = value

    def delete(self, key):
        self._cache.pop(key, None)

    def __del__(self):
        MockClient.number_of_clients -= 1

class RedisClusterConnectionTest(TestCase):

    def test_port(self):
        backend = MockRedisClusterBackend(arguments={"host": "foo", "port": 6789})
        eq_(backend._create_client().arg[2], 6789)

    def test_host(self):
        backend = MockRedisClusterBackend(arguments={"host": "foo"})
        eq_(backend._create_client().arg[0], "foo")

    def test_url(self):
        backend = MockRedisClusterBackend(arguments={"url": "foo"})
        eq_(backend._create_client().arg[0], "foo")